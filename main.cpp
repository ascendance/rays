#include <cstdio>
#include <ctime>

#include "camera.h"

int main(int argc, char ** argv)
{
	clock_t tm;
	World MakeBelieve(20, 5);

	/* Adding scenery */

	//6x walls

	pv3 wallnormal;
	pv3 wallcenter;

	int gifframes;

	Material m;
	Wall * current;

	//1
	m = Material(pv3(255.0, 0, 0)); 
	m.reflection = 0.99;
	m.absorption = 0.0;
	wallnormal = pv3(0, 0, 1);
	wallcenter = pv3(0, 0, -10);
	current = new Wall;
	*current = Wall(m, wallnormal, wallcenter);
	MakeBelieve.AddObject(current);
	
	//2
	m = Material(pv3(255.0, 0, 0)); 
	m.reflection = 0.99;
	m.absorption = 0.0;
	wallnormal = pv3(0, 0, -1);
	wallcenter = pv3(0, 0, 10);
	current = new Wall;
	*current = Wall(m, wallnormal, wallcenter);
	MakeBelieve.AddObject(current);

	//3
	m = Material(pv3(255.0, 0, 255.0)); 
	//m.reflection = 0.7;
	m.absorption = 0.3;
	wallnormal = pv3(0, 1, 0);
	wallcenter = pv3(0, -10, 0);
	current = new Wall;
	*current = Wall(m, wallnormal, wallcenter);
	MakeBelieve.AddObject(current);

	//4
	m = Material(pv3(255.0, 0, 255.0)); 
	//m.reflection = 0.7;
	m.absorption = 0.3;
	wallnormal = pv3(0, -1, 0);
	wallcenter = pv3(0, 10, 0);
	current = new Wall;
	*current = Wall(m, wallnormal, wallcenter);
	MakeBelieve.AddObject(current);

	//5
	m = Material(pv3(255.0, 0, 0));
	m.reflection = 0.0;
	m.absorption = 0.9;
	m.SetTexture(dots, 2.0);
	m.SetBumpMap(bumps, 2.0);
	wallnormal = pv3(1, 0, 0);
	wallcenter = pv3(-10, 0, 0);
	current = new Wall;
	*current = Wall(m, wallnormal, wallcenter);
	MakeBelieve.AddObject(current);

	//6
	m = Material(pv3(255.0, 0, 0)); 
	m.reflection = 0.99;
	m.absorption = 0.0;
	wallnormal = pv3(-1, 0, 0);
	wallcenter = pv3(10, 0, 0);
	current = new Wall;
	*current = Wall(m, wallnormal, wallcenter);
	MakeBelieve.AddObject(current);


	//some balls
	pv3 ballcenter;
	double ballradius;

	m = Material(pv3(0.0, 0, 255.0));
	m.reflection = 0.3;
	m.absorption = 0.7;
	m.lightmodel = MINNAERT;
	ballradius = 4.0;
	ballcenter = pv3(6.0, 6.0 ,6.0);
	Ball * currball;
	currball = new Ball;
	*currball = Ball(m, ballcenter, ballradius);
	MakeBelieve.AddObject(currball);
	
	m = Material(pv3(0.0, 255.0, 0.0));
	m.reflection = 0.3;
	m.absorption = 0.7;
	m.SetBumpMap(waves, 0.03);
	m.lightmodel = RIM;
	ballradius = 4.0;
	ballcenter = pv3(6.0, -6.0 ,6.0);
	currball = new Ball;
	*currball = Ball(m, ballcenter, ballradius);
	MakeBelieve.AddObject(currball);
		
	m = Material(pv3(255.0, 0, 0.0));
	m.reflection = 0.3;
	m.absorption = 0.7;
	m.lightmodel = WARD;
	ballradius = 4.0;
	ballcenter = pv3(-6.0, 6.0 ,6.0);
	currball = new Ball;
	*currball = Ball(m, ballcenter, ballradius);
	MakeBelieve.AddObject(currball);
	
	m = Material(pv3(255.0, 255.0, .0));
	m.reflection = 0.3;
	m.absorption = 0.7;
	m.lightmodel = FONG;
	ballradius = 4.0;
	m.SetTexture(dots, 0.05);
	m.SetBumpMap(bumps, 0.05);
	ballcenter = pv3(-6.0, -6.0 ,6.0);
	currball = new Ball;
	*currball = Ball(m, ballcenter, ballradius);
	MakeBelieve.AddObject(currball);
	
	/* setting light source */
	pv3 source;
	double power;

	power = 3.0;
	source = pv3(-2.0, 4.0, -6.0);
	MakeBelieve.AddLightSource(source, power);

	power = 1.5;
	source = pv3(3.0, -7.0, -4.0);
	MakeBelieve.AddLightSource(source, power);


/*
	power = 0.45;
	source = pv3(9.0, 9.0, 0.0);
	MakeBelieve.AddLightSource(source, power);
	source = pv3(-9.0, 9.0, 0.0);
	MakeBelieve.AddLightSource(source, power);
	source = pv3(9.0, -9.0, 0.0);
	MakeBelieve.AddLightSource(source, power);
	source = pv3(-9.0, -9.0, 0.0);
	MakeBelieve.AddLightSource(source, power);
	*/



	/* setting camera */
		
	pv3 cameracenter(0, 0, -9);
	pv3 camerasight(0, 0, 1);
	pv3 cameratop(0, 1, 0);

	Camera Ward(cameracenter, camerasight, cameratop, 1920, 1920, M_PI * 7.0 / 12.0);

	tm = clock();

	/*
	Ward.InitGif("gifka.gif");
	
	gifframes = 5;	

	for(int j = 0; j < gifframes; j++)
	{
		printf("Rendering GIF frames: %d out of %d\n", j, gifframes);
		cameracenter.z = - 9 + j * 1;
		Ward.MoveCamera(cameracenter, camerasight, cameratop);
		Ward.BuildImage(MakeBelieve, 5);
		Ward.AddFrame();
	};

	Ward.FinishGif();	*/

	Ward.BuildImage(MakeBelieve, 5);
	Ward.SaveToPng("picture.png");
	
	tm = clock() - tm;
	
	printf("Rendering time: %f.\n", ((float)tm) / CLOCKS_PER_SEC);

};
