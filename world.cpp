#include "raypv3.h"
#include "lightmodels.h"
#include "object.h"
#include "world.h"

#include <cmath>

World::World(int maxobj, int maxlights)
{
	objectcount= 0;
	maxobjects = maxobj;
	objects = new Object*[maxobj];

	lightsources = 0;
	maxlightsources = maxlights;
	lights = new pv3[maxlights];
	lightpowers = new double[maxlights];
};

bool World::AddObject(Object * ootka)
{
	if (objectcount >= maxobjects) return false;

	objects[objectcount] = ootka;
	objectcount++;
	return true; 
};

bool World::AddLightSource(pv3 & coord, double power)
{
	if (lightsources >= maxlightsources) return false;

	lights[lightsources] = coord;
	lightpowers[lightsources] = power;
	lightsources++;
};

//result is the pixel color
pv3 World::RayTrace(Ray & ray, int recursion) const
{
	double dist;
	pv3 point;
	pv3 collisionpoint;

	pv3 fullcolor = pv3(0, 0, 0);

	double tmp;

	int fcollision;

	dist = 10000; //infinity

	//searching for the first collision of the ray
	fcollision = -1;
	for(int i = 0; i < objectcount; i++)
	{
		if (objects[i]->Intersect(ray, point, tmp))
		{
			if(tmp < dist)
			{
				dist = tmp;
				fcollision = i;
				collisionpoint = point;
			};
		};
	};		

	//if there was a collision at all
	if (fcollision >= 0)
	{
		pv3 nrm, lgt;
		double lighting;
		double disttolight;
		Ray tolight;
		pv3 obstacle;
		int iobj;
		double lightloss;
		double fogfactor;
		double fogexp;

		pv3 reflectedcolor;
		pv3 internalcolor;
		pv3 refractedcolor;
		pv3 fogcolor;

		//calculating normal in the intersection point
		nrm = objects[fcollision]->Normal(collisionpoint);

		//calculating total lighting power of the pixel

		lighting = 0;			

		//drawing a line to all light sources
		for(int i = 0; i < lightsources; i++)
		{
			//checking if the light source is obstacled
			lgt = lights[i] - collisionpoint;
			disttolight = sqrt(lgt * lgt);
			lgt = lgt * (1.0 / disttolight); 

			//building a ray towards a light source
			tolight.start = collisionpoint;
			tolight.direction = lgt;

			//searching for a closer intersection than the light source
			for(iobj = 0;iobj  < objectcount; iobj++)
			{
				if (objects[iobj] -> Intersect(tolight, obstacle, tmp))
				{
					if (tmp < disttolight)
					{
						break;
					};
				};	
			};	
			//if an obstacle was found, skipping this lightsource	
			if (iobj != objectcount)
			{
				continue;
			};

			//adjusting lighting from this particular light source
			//searching for the light loss according to the travelled distance
			lightloss = 1.0 / (1.0 + disttolight * disttolight * 0.01);

			//using the collision object material lighting model	
			lighting += lightpowers[i] * CalcLightPower(objects[fcollision]->m.lightmodel, ray.direction, nrm, lgt) * lightloss;
		};

		if (lighting > 1.0) lighting = 1.0;

		//searching for the color as sum of abs, refl and fog parts

		//checking for recursive possibility			
		if (recursion > 0)
		{
			//reflection
			if (objects[fcollision]->m.reflection > EPS)
			{
				Ray reflected;

				reflected.start = collisionpoint;
				reflected.direction = ray.direction - nrm * (ray.direction * nrm) * 2.0;					

				//++reflected.direction;

				reflectedcolor = RayTrace(reflected, recursion - 1);
			};

			//refraction
			if (objects[fcollision]->m.refraction > EPS)
			{
				Ray refracted;
				double fall;
				double refr;

				refracted.start = collisionpoint;

				fall = ray.direction * nrm;

				if (fall <= 0)
				{
					//Ray is coming from outside of the object
					refr = sqrt(1.0 - fall * fall);
					refr = refr * 1.0 / objects[fcollision]->m.reffactor;

					refr = -sqrt(1.0 - refr * refr);

					refracted.direction = ray.direction + nrm * (refr - fall);

					refractedcolor = RayTrace(refracted, recursion - 1);
				}
				else
				{
					//Ray is coming from inside of the object

					//checking for full reflection criteria

					refr = sqrt(1.0 - fall * fall);
					refr = refr * objects[fcollision]->m.reffactor;

					if (refr > 1) 
					{
						refractedcolor = pv3(0, 0, 0);
					}
					else
					{
						refr = -sqrt(1.0 - refr * refr);

						refracted.direction = ray.direction + nrm * (refr - fall);

						refractedcolor = RayTrace(refracted, recursion - 1);
					};
				};							

			};
		};

		//absorption
		//internalcolor = objects[fcollision]-> m.color;
		internalcolor = objects[fcollision]->getColor(collisionpoint);

		//fog
		fogexp = 0.02;
		fogfactor = 1.0 - exp(- fogexp * dist);
		fogcolor = pv3(150.0, 150.0, 150.0);

		fullcolor = ((internalcolor * objects[fcollision]->m.absorption + reflectedcolor * objects[fcollision]->m.reflection + refractedcolor * objects[fcollision]->m.refraction) * (1.0 - fogfactor) + fogcolor * fogfactor) * lighting;
	};

	return fullcolor;	
};	

World::~World()
{
	for(int i = 0; i < objectcount; i++)
	{
		delete objects[i];
	};

	delete[] lights;
	delete[] lightpowers;	
	delete[] objects;
};


