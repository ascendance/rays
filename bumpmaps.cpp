#include "raypv3.h"
#include <cmath>

//actual function:
//sin(x * 2 * M_PI);
pv3 waves(double x, double y)
{
	pv3 res;

	//x deriv
	res.x =  0.3 * M_PI * cos(y * 2.0 * M_PI);
	res.y = 0;
	
	return res;
};

//actual function:
//0 outside of (x^2 + y^2 < r * r)
pv3 bumps(double x, double y)
{
	pv3 res;
	double r = 0.4;

	double xt, yt;
	double tmp, dst;

	xt = x - 0.5;
	yt = y - 0.5;

	if (xt * xt + yt * yt >= r * r)
	{
		res.x = 0;
		res.y = 0;	
	}
	else
	{
		dst = sqrt(xt * xt + yt * yt);
		tmp = - sin (M_PI * dst / r) * M_PI / (dst * r);

		res.x = tmp * xt / 6.0;
		res.y = tmp * yt / 6.0;
	};
	
	return res;
};
