#include "raypv3.h"

pv3 dots(double x, double y)
{
	double r = 0.4;
	if ((x - 0.5) * (x - 0.5) + (y - 0.5) * (y - 0.5) < r * r)
	{
		return pv3(0, 0, 255.0);
	}
	else
	{
		return pv3(255.0, 255.0, 255.0);
	};
};
