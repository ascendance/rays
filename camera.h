#include <cmath>

#include "raypv3.h"
#include "lightmodels.h"
#include "object.h"
#include "world.h"
#include "textures.h"
#include "bumpmaps.h"

#define STB_IMAGE_WRITE_IMPLEMENTATION
#include "stbwrite.h"

#include "gif.h"

class Camera
{
public:
	int xtics;
	int ytics;

	pv3 eye;
	pv3 sightline;

	pv3 xdirection;
	pv3 ydirection;

	double xangle;
	double yangle;

	pv3 * colors;

	GifWriter gwr;

	Camera()
	{
	};

	Camera(const pv3 & seteye, const pv3 & setdirection, const pv3 & top, const int setxtics = 1024, const int setytics = 1024, const double setxangle = M_PI * 7.0/ 12.0, const double setyangle = - 1.0)
	{
		
		eye = seteye;
		sightline = setdirection;
		++sightline;
		
		xtics = setxtics;
		ytics = setytics;

		xangle = setxangle;

		if (setyangle <= 0)
		{
			yangle = xangle * ((double) ytics) / xtics;
		};

		ydirection = top - sightline * (sightline * top);
		++ydirection;

		xdirection = sightline ^ top;
		++xdirection;

		colors = new pv3[xtics * ytics];
		
	};

	int BuildImage(const World & w, int recursionlevel)
	{
		double alpha, beta;
				

		Ray pixel;
		pixel.start = eye;
	
		alpha = tan(xangle / 2.0);
		beta = tan(yangle / 2.0);

		for(int xtic = 0; xtic < xtics; xtic++)
		{
			for(int ytic = 0; ytic < ytics; ytic++)
			{		
				pixel.direction = xdirection * alpha * (-1.0 + 2.0 * (double) xtic / xtics)  + ydirection * beta * (- 1.0 + 2.0 * (double) ytic / ytics ) + sightline;
				++pixel.direction;
				colors[xtic + ytic * xtics] = w.RayTrace(pixel, recursionlevel);
			};
		};

		/*
		for(int xtic = 0; xtic < xtics; xtic++)
		{
			for(int ytic = 0; ytic < ytics; ytic++)
			{
				
				colors[xtic + ytic * xtics].x = (double) xtic / xtics * 255.0;
				colors[xtic + ytic * xtics].y = (double) ytic / ytics * 255.0;
			};
		};*/
	};

	int SaveToPng(char * output)
	{
		unsigned char * data = new unsigned char[3 * xtics * ytics];

		for(int i = 0; i < xtics * ytics; i++)
		{
			data[i * 3 + 0] = (unsigned char) colors[i].x;
			data[i * 3 + 1] = (unsigned char) colors[i].y;
			data[i * 3 + 2] = (unsigned char) colors[i].z;
		};
		
		stbi_write_png(output, xtics, ytics, 3, data, xtics * 3);
	
		delete[] data;
	}; 

	int InitGif(char * output)
	{
		GifBegin(&gwr, output, xtics, ytics, 3);
	};

	int AddFrame()
	{
		unsigned char * data = new unsigned char[4 * xtics * ytics];

		for(int i = 0; i < xtics * ytics; i++)
		{
			data[i * 4 + 0] = (unsigned char) colors[i].x;
			data[i * 4 + 1] = (unsigned char) colors[i].y;
			data[i * 4 + 2] = (unsigned char) colors[i].z;
			data[i * 4 + 3] = 0;
		};
		
		GifWriteFrame(&gwr, data, xtics, ytics, 3);
	};

	int FinishGif()
	{
		GifEnd(&gwr);
	}; 

	int MoveCamera(pv3 neweye, pv3 newdirection, pv3 top = pv3(0.0, 1.0, 0.0))
	{
		eye = neweye;
		sightline = newdirection;
		++sightline;

		ydirection = top - sightline * (sightline * top);
		++ydirection;

		xdirection = sightline ^ top;
		++xdirection;
	};
	
	~Camera()
	{
		delete[] colors;
	};
};
