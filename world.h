
struct World
{
	//all world objects
	int objectcount;
	int maxobjects;
	Object ** objects;

	int lightsources;
	int maxlightsources;
	pv3 * lights;
	double * lightpowers;

	World(int maxobj, int maxlights);
		
	bool AddObject(Object * ootka);
	bool AddLightSource(pv3 & coord, double power);
	
	//result is the pixel color
	pv3 RayTrace(Ray & ray, int recursion) const;
	
	~World();
};


