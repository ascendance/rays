
CPP = icpc
FLAGS = -O3

compile: raypv3.o object.o bumpmaps.o world.o lightmodels.o textures.o
	$(CPP) $(FLAGS) main.cpp raypv3.o object.o bumpmaps.o world.o lightmodels.o textures.o -o exec

run:
	./exec

clean:
	rm *.o
	rm *exec

raypv3.o: raypv3.cpp raypv3.h
	$(CPP) $(FLAGS) raypv3.cpp -c -o raypv3.o

object.o: object.cpp object.h
	$(CPP) $(FLAGS) object.cpp -c -o object.o

bumpmaps.o: bumpmaps.cpp bumpmaps.h
	$(CPP) $(FLAGS) bumpmaps.cpp -c -o bumpmaps.o

world.o: world.cpp world.h
	$(CPP) $(FLAGS) world.cpp -c -o world.o

lightmodels.o: lightmodels.cpp lightmodels.h
	$(CPP) $(FLAGS) lightmodels.cpp -c -o lightmodels.o

textures.o: textures.cpp textures.h
	$(CPP) $(FLAGS) textures.cpp -c -o textures.o

