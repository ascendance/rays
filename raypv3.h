#define EPS 1.0e-10

struct pv3
{
	double x, y, z;

	pv3(double xs, double ys, double zs);
	pv3();

	pv3 operator*(const double & sc);

	//scalar product
	double operator*(const pv3 & b) const;
	pv3 operator+(const pv3 & b) const;
	pv3 operator-(const pv3 & b) const;
	pv3 & operator= (const pv3 & lul);

	//prefix ++ is for normalization of a vector
	pv3 & operator++();

	//vector product
	pv3 operator^ (const pv3 & lul) const;

	~pv3();
};

struct Ray
{
public:
	pv3 start;
	pv3 direction;

	Ray();
	Ray(pv3 s, pv3 d);
	double DistFromPoint(const pv3 & point, pv3 & closest);
};


