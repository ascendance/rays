#include <cmath>

#include "raypv3.h"
#include "lightmodels.h"

double CalcLightPower(int model, pv3 & eye, pv3 & nrm, pv3 & lgt)
{
	if (model == LAMBERTH)
	{
		double tmp;
		tmp =  (lgt * nrm);
		if (tmp < 0) tmp = 0;
		return tmp;
	};

	if (model == FONG)
	{
		double diff;
		double spec;

		pv3 r;
		r = eye - nrm * (nrm * eye) * 2.0;
		spec = r * lgt;
		if (spec < 0) spec = 0;
		
		spec = pow(spec, 5.0);
		
		diff = (lgt * nrm);
		if (diff < 0) diff = 0;
		
		return (diff + spec) / 2;
		
	};

	if (model == RIM)
	{
		double bconst = 0.2;
		double specpower = 10;
	
		double diff, spec;

		diff = (lgt * nrm);
		if (diff < 0) diff = 0;

		spec = - (eye * nrm);
	 	if (spec < 0) spec = 0;

		spec = 1 + bconst - spec;

		if (spec > 1) spec = 1;

		spec =pow(spec, specpower);
	
		return (spec + diff) / 2.0;
	};

	if (model == WARD)
	{
		double diff, spec;
		double k = 5.0;
		pv3 ash;
		double hn;
		
		diff = (nrm * lgt);
		if (diff < 0) diff = 0;

		ash = lgt - eye;
		++ash;

		hn = ash * nrm;

		if (hn < EPS)
		{
			spec = 0.0;
		}
		else
		{
			spec = exp( - k * (1.0 - hn * hn) / (hn * hn));
		};

		return (diff + spec) / 2.0;
	};

	if(model == MINNAERT)
	{
		double k = 0.8;
		return pow((nrm * lgt), 1.0 + k) * pow(1.0 + nrm * eye, 1.0 - k);
		
	};


};
