#include <cstdio>
#include <cmath>
#include "raypv3.h"

pv3::pv3(double xs, double ys, double zs)
{
	x = xs;
	y = ys;
	z = zs;
};

pv3::pv3()
{
	x = 0;
	y = 0;
	z = 0;
};

pv3 pv3::operator*(const double & sc)
{
	pv3 NW;
	NW.x = x * sc;
	NW.y = y * sc;
	NW.z = z * sc;
	return NW;
};

//scalar product
double pv3::operator*(const pv3 & b) const
{
	return x * b.x + y * b.y + z * b.z;	
};

pv3 pv3::operator+(const pv3 & b) const
{
	pv3 NW;
	NW.x = x + b.x;
	NW.y = y + b.y;
	NW.z = z + b.z;
	return NW;
};

pv3 pv3::operator-(const pv3 & b) const
{
	pv3 NW;
	NW.x = x - b.x;
	NW.y = y - b.y;
	NW.z = z - b.z;
	return NW;
};

pv3 & pv3::operator= (const pv3 & lul)
{
	x = lul.x;
	y = lul.y;
	z = lul.z;
	return *this;
};

//prefix ++ is for normalization of a vector
pv3 & pv3::operator++()
{
	double nrm;
	nrm = sqrt(x * x + y * y + z * z);
	if (nrm < EPS) printf("Error: normalization of a zero vector.\n");
	x /= nrm;
	y /= nrm;
	z /= nrm;
	return *this;	
};

//vector product
pv3 pv3::operator^ (const pv3 & lul) const
{
	pv3 NW;
	NW.x = y * lul.z - z * lul.y;
	NW.y = z * lul.x - x * lul.z;
	NW.z = x * lul.y - y * lul.x;
	return NW;
};

pv3::~pv3()
{
};

Ray::Ray()
{
	pv3 s, d;
	start =  s;
	direction = d;
};

Ray::Ray(pv3 s, pv3 d)
{
	start = s;
	direction = ++d;
};

double Ray::DistFromPoint(const pv3 & point, pv3 & closest)
{
	double t;
	pv3 tmp;

	t =  (start - point) * direction;
	t = -t;
	if (t < 0) t = 0;

	closest = start  + direction * t;

	tmp = closest - point;

	return sqrt(tmp * tmp);
};


