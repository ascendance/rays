
struct Material
{
	pv3 color;

	//part of the light that will be taken from the object itself
	double absorption;
	
	//part of the light that will be taken from the reflected ray (1 <> mirror)
	double reflection;

	//part of the refracted light
	double refraction;
	double reffactor;

	int lightmodel;

	bool isTextured;
	pv3 (*texture)(double, double);
	double tscale;

	bool isBumped;
	pv3 (*bumpMap)(double, double);
	double bscale;

	Material(pv3 clr = pv3(255.0, 0.0, 0.0), double abs = 1.0, double refl = 0.0, double refr = 0.0, double rff = 1.3, int lm = FONG);
	
	void SetTexture(pv3 (*LUL)(double, double), double scale);
	
	void SetBumpMap(pv3 (*LUL)(double, double), double scale);
};

class Object
{
public:
	Material m;
	
	//these functions should be present for each object type:

	//checks for intersection with arbitrary ray and return collision point and distance from ray origin
	virtual bool Intersect(Ray r, pv3 & pt, double & dist);	

	//returns 2-dimensional texture coordinate numbers in 0:1, z is unused
	virtual pv3 TextureCoord(pv3 & pt, double scale);

	//return a vector along texture first coordinate 
	virtual pv3 Tangent(pv3 & pt);

	//returns external normal to object at any given point of the object
	virtual pv3 TrueNormal(pv3 & pt);

	//interface for color checking
	pv3 getColor(pv3 pt);
	
	//interface for bump mapping
	pv3 Normal(pv3 & pt);
};

/* different types of objects listed here */
class Wall : public Object
{
public:
	pv3 center;
	pv3 normal;

	//normalized texture align vectors
	pv3 tex1, tex2;

	Wall();

	Wall(Material mat, pv3 n, pv3 c, pv3 TextureTop = pv3(1.0, 1.0, 1.0));
	
	virtual bool Intersect(Ray r, pv3 & pt, double & dist);
		
	virtual pv3 TrueNormal(pv3 & pt);
	
	virtual pv3 TextureCoord(pv3 & pt, double scale);

	virtual pv3 Tangent(pv3 & pt);
};

class Ball : public Object
{
public:
	pv3 center;
	double radius;

	Ball();

	Ball(Material mat, pv3 c, double r);
	
	virtual bool Intersect(Ray r, pv3 & pt, double & dist);
	
	virtual pv3 TrueNormal(pv3 & pt);

	virtual pv3 TextureCoord(pv3 & pt, double scale);
	
	virtual pv3 Tangent(pv3 & pt);
};


